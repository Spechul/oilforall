﻿using System;
using System.Collections.Generic;
using LasLibrary.DefaultLasWriter;
using NUnit.Framework;

namespace Framework.Test.LasLibrary
{
    [TestFixture]
    public class LasWriterTests
    {
        [Theory]
        public void LasWriter_ShouldWriteVersion()
        {
            var expected = new List<string>()
            {
                "~V ", "VERS. 2.0 : CWLS LOG ASCII STANDARD - VERSION 2.0", "WRAP. NO : One line per depth step"
            };
            var sm = new StreamMock();
            sm.CurrentSection = 'V';
            using (var lw = new LasWriter(sm))
            {
                lw.WriteVersionInformation();
                var count = sm.Sections['V'].Count;
                var enumerators = new IEnumerator<string>[2];
                try
                {
                    enumerators[0] = sm.Sections['V'].GetEnumerator();
                    enumerators[1] = expected.GetEnumerator();
                    for (int i = 0; i < count; i++)
                    {
                        enumerators[0].MoveNext();
                        enumerators[1].MoveNext();
                        Assert.AreEqual(enumerators[0].Current, enumerators[1].Current);
                    }
                }
                finally
                {
                    foreach (var enumerator in enumerators)
                    {
                        enumerator.Dispose();
                    }
                }
            }
        }

        [Theory]
        [TestCase("header", "m", "m", "m", "comp", "well", "fld", "loc", "prov", "sc")]
        public void LasWriter_ShouldWriteWellInfoStrings(params string[] values)
        {
            var sm = new StreamMock();
            sm.CurrentSection = 'W';
            using (LasWriter lw = new LasWriter(sm))
            {
                lw.WellHeader = values[0];
                lw.StartDepth = new LasMeasurement {Value = 0, Uom = values[1]};
                lw.StopDepth = new LasMeasurement {Value = 0, Uom = values[2]};
                lw.StepDepth = new LasMeasurement {Value = 0, Uom = values[3]};
                lw.Company = values[4];
                lw.Well = values[5];
                lw.Field = values[6];
                lw.Location = values[7];
                lw.District = values[8];
                lw.ServiceCompany = values[9];

                lw.WriteWellInformation();
                var arr = sm.Sections['W'].ToArray();
                Assert.True(arr[0].Contains(values[0]));
                Assert.True(arr[1].Contains(values[1]));
                Assert.True(arr[2].Contains(values[2]));
                Assert.True(arr[3].Contains(values[3]));
                Assert.True(arr[5].Contains(values[4]));
                Assert.True(arr[6].Contains(values[5]));
                Assert.True(arr[7].Contains(values[6]));
                Assert.True(arr[8].Contains(values[7]));
                Assert.True(arr[9].Contains(values[8]));
            }
        }
    }
}