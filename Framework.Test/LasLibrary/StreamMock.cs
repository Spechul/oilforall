﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Framework.Test.LasLibrary
{
    public class StreamMock : TextWriter
    {
        public override Encoding Encoding { get; }


        public Dictionary<char, List<string>> Sections = new Dictionary<char, List<string>>();

        public char CurrentSection { get; set; } = 'V';

        public StreamMock()
        {
            Sections['V'] = new List<string>();
            Sections['W'] = new List<string>();
            Sections['C'] = new List<string>();
            Sections['P'] = new List<string>();
            Sections['O'] = new List<string>();
            Sections['A'] = new List<string>();
        }


        public override void WriteLine(string value)
        {
            Sections[CurrentSection].Add(value);
        }
    }
}