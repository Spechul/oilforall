﻿using System;
using System.Globalization;

namespace LasLibrary.DefaultLasReader
{
    public struct LasEntry
    {
        public string Data;
        public string Mnem;
        public string Units;
        public string Comment;

        public override string ToString()
        {
            return $"{Mnem}.{Units} {Data} : {Comment}";
        }
    }
}