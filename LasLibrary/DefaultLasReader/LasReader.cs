﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace LasLibrary.DefaultLasReader
{
    public class LasReader : IDisposable
    {
        #region Constants

        public const char VersionInformation = 'V';
        public const char WellInformation = 'W';
        public const char CurveInformation = 'C';
        public const char ParameterInformation = 'P';
        public const char OtherInformation = 'O';
        public const char AsciiLogInformation = 'A';

        #endregion


        #region Fields

        // it's like Mnem.Unit Data(anything except last `:`):Description
        private readonly Regex _entryMatcher = new Regex(@"^(\w*)\s*\.([^\s]*)\s(.*)(?::)+(.*)$");

        private Stream _stream;
        private StreamReader _reader;
        private Dictionary<char, bool> _sectionRead = new Dictionary<char, bool>
        {
            { VersionInformation, false },
            { WellInformation, false },
            { CurveInformation, false },
            { ParameterInformation, false },
            { OtherInformation, false },
            { AsciiLogInformation, false }
        };

        private LasString? _lastRead = null;

        #endregion


        #region Properties

        public CultureInfo Culture { get; protected set; } = CultureInfo.InvariantCulture;
        public Dictionary<string, List<double>> AsciiLogInformationEntries { get; protected set; } = new Dictionary<string, List<double>>();
        public List<LasEntry> VersionInformationEntries { get; protected set; } = new List<LasEntry>();
        public List<LasEntry> WellInformationEntries { get; protected set; } = new List<LasEntry>();
        public List<LasEntry> ParameterInformationEntries { get; protected set; } = new List<LasEntry>();
        public List<LasEntry> CurveInformationEntries { get; protected set; } = new List<LasEntry>();
        public string OtherInformationEntry { get; protected set; }

        #endregion


        #region Constructors

        public LasReader(string path)
        {
            _stream = new FileStream(path, FileMode.Open);
            _reader = new StreamReader(_stream);
        }

        public LasReader(string path, CultureInfo culture) : this(path)
        {
            Culture = culture;
        }

        #endregion


        #region Public Methods

        public string ReadTopDown()
        {
            var str = _reader.ReadToEnd();
            ResetReader();
            return str;
        }

        public void ReadVersionInformation(bool fromBeginning = false)
        {
            if (_sectionRead[VersionInformation])
                return;

            if (fromBeginning)
                ResetReader();

            SkipUntil(VersionInformation);

            ReadEntriesSection(VersionInformationEntries);

            _sectionRead[VersionInformation] = true;
        }

        public void ReadWellInformation(bool fromBeginning = false)
        {
            if (_sectionRead[WellInformation])
                return;

            if (fromBeginning)
                ResetReader();
            
            SkipUntil(WellInformation);

            ReadEntriesSection(WellInformationEntries);

            _sectionRead[WellInformation] = true;
        }

        public void ReadParameterInformation(bool fromBeginning = false)
        {
            if (_sectionRead[ParameterInformation])
                return;

            if (fromBeginning)
                ResetReader();

            SkipUntil(ParameterInformation);

            ReadEntriesSection(ParameterInformationEntries);

            _sectionRead[ParameterInformation] = true;
        }

        public void ReadCurveInformation(bool fromBeginning = false)
        {
            if (_sectionRead[CurveInformation])
                return;

            if (fromBeginning)
                ResetReader();

            SkipUntil(CurveInformation);

            ReadEntriesSection(CurveInformationEntries);

            _sectionRead[CurveInformation] = true;
        }

        public void ReadOtherInformation(bool fromBeginning = false)
        {
            if (_sectionRead[OtherInformation])
                return;

            if (fromBeginning)
                ResetReader();

            SkipUntil(OtherInformation);

            var strb = new StringBuilder();

            while (true)
            {
                ReadNextLine();

                if (!_lastRead.HasValue || _lastRead.Value.IsHeader)
                    break;

                strb.AppendLine(_lastRead.Value.Value);
            }

            OtherInformationEntry = strb.ToString();

            _sectionRead[OtherInformation] = true;
        }

        public void ReadAsciiLogInformation(bool fromBeginning = false)
        {
            if (_sectionRead[AsciiLogInformation])
                return;

            if (fromBeginning)
                ResetReader();

            SkipUntil(AsciiLogInformation);

            foreach (var curveInformationEntry in CurveInformationEntries)
            {
                AsciiLogInformationEntries[curveInformationEntry.Mnem] = new List<double>();
            }

            while (true)
            {
                ReadNextLine();

                if (!_lastRead.HasValue || _lastRead.Value.IsHeader)
                    break;

                var row = _lastRead.Value.Value.Split();
                int i = 0;
                foreach (var curveInformationEntry in CurveInformationEntries)
                {
                    AsciiLogInformationEntries[curveInformationEntry.Mnem].Add(double.Parse(row[i], Culture));
                    i++;
                }
            }

            _sectionRead[AsciiLogInformation] = true;
        }

        public void Dispose()
        {
            _reader?.Dispose();
            _stream?.Dispose();
        }

        #endregion

        #region Private Methods

        private void ReadEntriesSection(List<LasEntry> entryListToFill)
        {
            while (true)
            {
                ReadNextLine();

                if (!_lastRead.HasValue || _lastRead.Value.IsHeader)
                    break;

                var str = _lastRead.Value.Value;
                var match = _entryMatcher.Match(str.Trim());
                entryListToFill.Add(new LasEntry
                {
                    Mnem = match.Groups[1].Value.Trim(),
                    Units = match.Groups[2].Value.Trim(),
                    Data = match.Groups[3].Value.Trim(),
                    Comment = match.Groups[4].Value.Trim()
                });
            }
        }

        private void SkipUntil(char header)
        {
            header = char.ToUpper(header);

            if (_reader.EndOfStream)
                throw new InvalidDataException("End of file encountered while skipping Las section. Try reading using fromBeginning=true");

            while (!_reader.EndOfStream)
            {
                if (!_lastRead.HasValue || !_reader.EndOfStream)
                    ReadNextLine();
                

                var str = _lastRead.Value.Value;

                if (str.StartsWith($"~{header}"))
                    break;

                if (!_lastRead.Value.IsHeader)
                    _lastRead = null;
            }
        }

        private void ResetReader()
        {
            _stream.Position = 0;
            _reader.DiscardBufferedData();
        }

        private void ReadNextLine()
        {
            LasString? lasString = null;
            string str = _reader.ReadLine();

            while (str != null && str.TrimStart().StartsWith("#"))
                str = _reader.ReadLine();

            if (str != null)
                lasString = new LasString { IsHeader = str.StartsWith("~"), Value = str };

            _lastRead = lasString;
        }

        #endregion

    }
}