﻿namespace LasLibrary.DefaultLasReader
{
    struct LasString
    {
        public string Value;
        public bool IsHeader;
    }
}