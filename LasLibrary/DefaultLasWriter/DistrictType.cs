﻿namespace LasLibrary.DefaultLasWriter
{
    public enum DistrictType
    {
        Province,
        County,
        State,
        Country
    }
}