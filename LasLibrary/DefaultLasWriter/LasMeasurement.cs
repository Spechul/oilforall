﻿using System.Globalization;

namespace LasLibrary.DefaultLasWriter
{
    public struct LasMeasurement
    {
        public double Value;
        public string Uom;

        public override string ToString()
        {
            return $"{Value:0.00} {Uom}";
        }
    }
}