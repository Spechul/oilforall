﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using LasLibrary.DefaultLasReader;

namespace LasLibrary.DefaultLasWriter
{
    /// <summary>
    /// feel free to inherit from this class and add default ways of adding parameters, infos and other things.
    /// </summary>
    public class LasWriter : IDisposable
    {
        #region Fiels

        protected readonly TextWriter _stream;
        protected int _count = int.MinValue;

        #endregion

        #region Properties

        #region Options

        public CultureInfo Culture { get; protected set; } = CultureInfo.InvariantCulture;

        /// <summary>
        /// default is "0.00". choose more suitable for your purpose if needed
        /// </summary>
        public string ValuesFormat { get; set; } = "0.00";

        #endregion

        #region Version Information

        public string VersionHeader { get; set; }
        public double Version { get; set; } = 2.0; // 2.0 by default
        public bool Wrap { get; set; } = false; // false by default

        #endregion

        #region Well Information

        public string WellHeader { get; set; }

        public LasMeasurement StartDepth { get; set; }
        public LasMeasurement StopDepth { get; set; }
        public LasMeasurement StepDepth { get; set; }

        public double NullValue { get; set; } = CommonNullValues.ThreeNinesTwentyFive;

        /// <summary>
        /// recommended to be 21 char of length
        /// </summary>
        public string Company { get; set; } = "company1";
        public string Well { get; set; } = "well1";
        public string Field { get; set; } = "field1";
        public string Location { get; set; } = "location1";
        public DistrictType DistrictType { get; set; } = DistrictType.Province;

        /// <summary>
        /// Country, Province, County or State name
        /// </summary>
        public string District { get; set; } = "province1";
        public string ServiceCompany { get; set; } = "sc1";
        public DateTime Date { get; set; } = DateTime.UtcNow;
        /// <summary>
        /// UWI or API number goes here
        /// </summary>
        public string UniqueWellId { get; set; } = "071CB361-12AF-4815-BB50-65807D64BE4A";

        public AreaType AreaType { get; set; } = AreaType.Common;
        public int LicenseNumber { get; set; }

        #endregion

        #region Curve Information

        public string CurveHeader { get; set; }
        private List<string> CurveInfo { get; set; } = new List<string>();

        #endregion

        #region Parameters

        public string ParametersHeader { get; set; }
        List<LasEntry> _parameters = new List<LasEntry>();

        #endregion

        #region Other information

        public string OtherInformationHeader { get; set; }
        public string OtherInformationBody { get; set; }

        #endregion

        #region ASCII Log Data

        public string AsciiLogHeader { get; set; }
        private List<List<double>> _values = new List<List<double>>();

        #endregion

        #endregion

        #region Private Methods



        #endregion

        #region Constructors

        public LasWriter(string path)
        {
            _stream = new StreamWriter(path);
        }

        public LasWriter(string path, CultureInfo culture)
        {
            _stream = new StreamWriter(path);
            Culture = culture;
        }

        /// <summary>
        /// constructor for tests
        /// </summary>
        /// <param name="stream"></param>
        public LasWriter(TextWriter stream)
        {
            _stream = stream;
        }

        public LasWriter(TextWriter stream, CultureInfo culture)
        {
            _stream = stream;
            Culture = culture;
        }

        #endregion

        #region Public Methods

        public void WriteVersionInformation()
        {
            _stream.WriteLine($"~V {VersionHeader}");
            _stream.WriteLine($"VERS. {Version.ToString("0.0", Culture)} : CWLS LOG ASCII STANDARD - VERSION {Version.ToString("0.0", Culture)}");
            if (Wrap)
                _stream.WriteLine("WRAP. YES : Multiple lines per depth step");
            else
                _stream.WriteLine("WRAP. NO : One line per depth step");
        }

        public void WriteWellInformation()
        {
            _stream.WriteLine($"~W {WellHeader}");
            _stream.WriteLine(new LasEntry {Mnem = "STRT", Units = StartDepth.Uom, Data = StartDepth.Value.ToString(ValuesFormat, Culture), Comment = "START DEPTH" });
            _stream.WriteLine(new LasEntry { Mnem = "STOP", Units = StopDepth.Uom, Data = StopDepth.Value.ToString(ValuesFormat, Culture), Comment = "STOP DEPTH" });
            _stream.WriteLine(new LasEntry { Mnem = "STEP", Units = StepDepth.Uom, Data = StepDepth.Value.ToString(ValuesFormat, Culture), Comment = "STEP DEPTH" });
            _stream.WriteLine(new LasEntry { Mnem = "NULL", Data = NullValue.ToString(ValuesFormat, Culture), Comment = "NULL VALUE" });
            _stream.WriteLine(new LasEntry { Mnem = "COMP", Data = Company, Comment = "COMPANY" });
            _stream.WriteLine(new LasEntry { Mnem = "WELL", Data = Well, Comment = "WELL" });
            _stream.WriteLine(new LasEntry { Mnem = "FLD", Data = Field, Comment = "FIELD" });
            _stream.WriteLine(new LasEntry { Mnem = "LOC", Data = Location, Comment = "LOCATION" });
            switch (DistrictType)
            {
                case DistrictType.Province:
                    _stream.WriteLine(new LasEntry { Mnem = "PROV", Data = District, Comment = "PROVINCE" });
                    break;
                case DistrictType.County:
                    _stream.WriteLine(new LasEntry { Mnem = "CNTY", Data = District, Comment = "COUNTY" });
                    break;
                case DistrictType.State:
                    _stream.WriteLine(new LasEntry { Mnem = "STAT", Data = District, Comment = "STATE" });
                    break;
                case DistrictType.Country:
                    _stream.WriteLine(new LasEntry { Mnem = "CTRY", Data = District, Comment = "COUNTRY" });
                    break;
                default:
                    throw new InvalidOperationException("no other district types supported");
            }

            _stream.WriteLine(new LasEntry { Mnem = "SRVC", Data = ServiceCompany, Comment = "SERVICE COMPANY" });
            _stream.WriteLine(new LasEntry { Mnem = "DATE", Data = Date.ToShortDateString(), Comment = "LOG DATE" });

            switch (AreaType)
            {
                case AreaType.Common:
                    _stream.WriteLine(new LasEntry { Mnem = "UWI", Data = UniqueWellId, Comment = "UNIQUE WELL ID" });
                    break;
                case AreaType.USA:
                    _stream.WriteLine(new LasEntry { Mnem = "API", Data = UniqueWellId, Comment = "API NUMBER" });
                    break;
                default:
                    throw new InvalidOperationException("no other area types supported");
            }

            _stream.WriteLine(new LasEntry { Mnem = "LIC", Data = LicenseNumber.ToString(), Comment = "LICENSE NUMBER" });
        }

        public void WriteCurveInformation()
        {
            _stream.WriteLine($"~C {CurveHeader}");
            foreach (var descriptor in CurveInfo)
            {
                _stream.WriteLine(descriptor);
            }
        }

        public void WriteParameterInformation()
        {
            _stream.WriteLine($"~P {ParametersHeader}");
            foreach (var parameter in _parameters)
            {
                _stream.WriteLine(parameter);
            }
        }

        public void WriteOtherInformation()
        {
            _stream.WriteLine($"~O {OtherInformationHeader}");
            _stream.WriteLine(OtherInformationBody);
        }

        public void WriteAsciiLogInformation()
        {
            var enumerators = new List<IEnumerator<double>>();
            try
            {
                foreach (var row in _values)
                {
                    enumerators.Add(row.GetEnumerator());
                }

                StringBuilder outer = new StringBuilder();

                for (int i = 0; i < _count; i++)
                {
                    StringBuilder inner = new StringBuilder();
                    foreach (var enumerator in enumerators)
                    {
                        enumerator.MoveNext();
                        inner.Append($"{enumerator.Current.ToString(ValuesFormat, Culture)} ");
                    }

                    outer.AppendLine(inner.ToString());
                }

                _stream.WriteLine($"~A {AsciiLogHeader}");
                _stream.Write(outer.ToString());
            }
            finally
            {
                foreach (var enumerator in enumerators)
                {
                    enumerator.Dispose();
                }
            }
        }

        public void WriteEverything()
        {
            WriteVersionInformation();
            WriteWellInformation();
            WriteCurveInformation();
            WriteParameterInformation();
            WriteOtherInformation();
            WriteAsciiLogInformation();
        }

        /// <summary>
        /// pass string that consists of section flag names (VWCPOA) to write them
        /// </summary>
        public void WriteSpecified(string order)
        {
            order = order.ToUpper();
            foreach (var symbol in order)
            {
                switch (symbol)
                {
                    case 'V':
                        WriteVersionInformation();
                        break;
                    case 'W':
                        WriteWellInformation();
                        break;
                    case 'C':
                        WriteCurveInformation();
                        break;
                    case 'P':
                        WriteParameterInformation();
                        break;
                    case 'O':
                        WriteOtherInformation();
                        break;
                    case 'A':
                        WriteAsciiLogInformation();
                        break;
                }
            }
        }

        /// <summary>
        /// adds curve descriptor and pack of values for it
        /// </summary>
        /// <param name="descriptor">format this string yourself, goes to ~C part</param>
        /// <param name="values">values for curve</param>
        public void AddCurve(string descriptor, IEnumerable<double> values)
        {
            CurveInfo.Add(descriptor);
            _values.Add(values.ToList());
        }

        /// <summary>
        /// use when the most basic specification needed
        /// </summary>
        /// <param name="measurementType"></param>
        /// <param name="uom"></param>
        /// <param name="data"></param>
        /// <param name="comment"></param>
        /// <param name="values"></param>
        public void AddCurve(string measurementType, string uom, string data, string comment, IEnumerable<double> values)
        {
            if (_count == int.MinValue)
                _count = values.Count();
            else if (_count != values.Count())
                throw new InvalidOperationException("all present data collections should be of equal size");

            CurveInfo.Add($"{measurementType}.{uom} {data}: {comment}");
            _values.Add(values.ToList());
        }

        /// <summary>
        /// use to add parameter as <see cref="LasEntry"/>
        /// </summary>
        /// <param name="formatted"></param>
        public void AddParameter(LasEntry entry)
        {
            _parameters.Add(entry);
        }

        /// <summary>
        /// add basic parameter
        /// </summary>
        /// <param name="mnemonic"></param>
        /// <param name="uom"></param>
        /// <param name="data"></param>
        /// <param name="comment"></param>
        public void AddParameter(string mnemonic, string uom, double data, string comment)
        {
            _parameters.Add(new LasEntry {Mnem = mnemonic, Units = uom, Data = data.ToString(ValuesFormat, Culture), Comment = comment});
        }

        #endregion

        public void Dispose()
        {
            _stream?.Dispose();
        }
    }
}
