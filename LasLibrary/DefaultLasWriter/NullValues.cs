﻿namespace LasLibrary.DefaultLasWriter
{
    /// <summary>
    /// common null values, used for las files
    /// </summary>
    public static class CommonNullValues
    {
        public const double FourNines = -9999;
        public const double FourNinesTwentyFive = -9999.25;
        public const double ThreeNinesTwentyFive = -999.25;
    }
}