﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LasLibrary.DefaultLasReader;
using LasLibrary.DefaultLasWriter;
using UnitSystemLibrary;

namespace OilForAll
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var lasWriter = new LasWriter(new StreamWriter("a.las"), CultureInfo.InvariantCulture))
            {
                lasWriter.AddParameter("asd", "qwe", 2.345, "asdqwe");
                var random = new Random();
                lasWriter.AddCurve("depth", "m", "", "use this as base", new List<double>() {random.NextDouble(), 12345.6789, random.Next(), random.Next()});
                lasWriter.AddCurve("speed", "m/s", "", "move speed", new List<double>() { random.Next(), random.Next(), random.Next(), random.Next() });
                lasWriter.OtherInformationBody = "dorova";
                lasWriter.WriteSpecified("vwca");
                //lasWriter.WriteEverything();
                // lasWriter.WriteVersionInformation();
                // lasWriter.WriteWellInformation();
                // lasWriter.WriteCurveInformation();
                // lasWriter.WriteParameterInformation();
                // lasWriter.WriteOtherInformation();
                // lasWriter.WriteAsciiLogInformation();
            }
            /*
            using (var lasReader = new LasReader("b.las", CultureInfo.InvariantCulture))
            {
                //Console.WriteLine(lasReader.ReadTopDown());
                lasReader.ReadVersionInformation(true);
                lasReader.ReadWellInformation(true);
                lasReader.ReadCurveInformation(true);
                lasReader.ReadParameterInformation(true);
                lasReader.ReadOtherInformation(true);
                lasReader.ReadAsciiLogInformation(true);
                foreach (var versionEntry in lasReader.VersionInformationEntries)
                {
                    Console.WriteLine(versionEntry);
                }

                Console.WriteLine();
                Console.WriteLine();

                foreach (var wellInformationEntry in lasReader.WellInformationEntries)
                {
                    Console.WriteLine(wellInformationEntry);
                }

                Console.WriteLine();
                Console.WriteLine();

                foreach (var curveInformationEntryEntry in lasReader.CurveInformationEntries)
                {
                    Console.WriteLine(curveInformationEntryEntry);
                }

                Console.WriteLine();
                Console.WriteLine();

                Console.WriteLine(lasReader.OtherInformationEntry);

                Console.WriteLine();
                Console.WriteLine();

                var mnem = "depth";
                Console.WriteLine(mnem);
                foreach (var d in lasReader.AsciiLogInformationEntries[mnem])
                {
                    Console.WriteLine(d);
                }
            }*/
            var uInfo = UnitsInformation.Instance;
            uInfo.Save();
            Console.WriteLine(uInfo.Uoms[uInfo.BaseUnitSystem]["length"]);

            Console.ReadLine();

        }
    }
}
