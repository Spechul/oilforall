﻿namespace UnitSystemLibrary
{
    /// <summary>
    /// some predefined constants. note, spaces are included
    /// </summary>
    public class UnitTypes
    {
        public const string Angle = "Angle";
        public const string Area = "Area";
        public const string DataTransferRate = "Data Transfer Rate";
        public const string DigitalStorage = "Digital Storage";
        public const string Duration = "Duration";
        public const string Energy = "Energy";
        public const string Force = "Force";
        public const string Frequency = "Frequency";
        public const string Length = "Length";
        public const string Mass = "Mass";
        public const string Power = "Power";
        public const string Pressure = "Pressure";
        public const string Speed = "Speed";
        public const string Temperature = "Temperature";
        public const string Time = "Time";
        public const string Volume = "Volume";
        public const string Weight = "Weight";
    }
}