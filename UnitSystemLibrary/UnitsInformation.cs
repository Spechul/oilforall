﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace UnitSystemLibrary
{
    public class UnitsInformation
    {

        public string BaseUnitSystem;


        #region Static Members

        private static UnitsInformation _instance;
        public static UnitsInformation Instance => _instance ?? (_instance = new UnitsInformation());

        #endregion

        #region Properties

        public Dictionary<string, Dictionary<string, string>> Uoms { get; protected set; } = new Dictionary<string, Dictionary<string, string>>();

        #endregion

        #region Constructors

        protected UnitsInformation()
        {
            BaseUnitSystem = "Metric";
            Uoms[BaseUnitSystem] = new Dictionary<string, string>
            {
                { "length", "m" },
                { "current", "A" },
            };
        }

        #endregion

        public void Save()
        {
            var str = JsonConvert.SerializeObject(this, Formatting.Indented);
            Console.WriteLine(str);
        }
    }
}
